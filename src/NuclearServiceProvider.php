<?php

/**
 * @file
 * Contains \Drupal\nuclear\RpnFieldServiceProvider.
 */

namespace Drupal\nuclear;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Nuclear module service provider.
 */
class NuclearServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->setDefinition('nuclear.original.module_handler', $container->getDefinition('module_handler'));
    $container->setDefinition('module_handler', $container->getDefinition('nuclear.new.module_handler'));
  }

}

