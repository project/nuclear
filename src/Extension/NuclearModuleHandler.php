<?php

namespace Drupal\nuclear\Extension;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Site\Settings;
use Drupal\nuclear\EntityTreeCrawler;
use Drupal\rpn_field\Rpn;

/**
 *
 */
class NuclearModuleHandler implements ModuleHandlerInterface {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var string
   */
  protected $hookPattern;

  /**
   * @var \Drupal\nuclear\EntityTreeCrawler
   */
  protected $crawler;

  /**
   * Constructs a rpn_field module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(ModuleHandlerInterface $module_handler, Settings $settings, EntityTreeCrawler $crawler) {
    $this->moduleHandler = $module_handler;
    $this->hookPattern = $settings->get('nuclear_hook_pattern', '/_(insert|update|delete|presave)$/');
    $this->crawler = $crawler;
  }

  /**
   * {@inheritdoc}
   */
  public function load($name) {
    return $this->moduleHandler->load($name);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAll() {
    return $this->moduleHandler->loadAll();
  }

  /**
   * {@inheritdoc}
   */
  public function isLoaded() {
    return $this->moduleHandler->isLoaded();
  }

  /**
   * {@inheritdoc}
   */
  public function reload() {
    return $this->moduleHandler->reload();
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleList() {
    return $this->moduleHandler->getModuleList();
  }

  /**
   * {@inheritdoc}
   */
  public function getModule($name) {
    return $this->moduleHandler->getModule($name);
  }

  /**
   * {@inheritdoc}
   */
  public function setModuleList(array $module_list = array()) {
    return $this->moduleHandler->setModuleList($module_list);
  }

  /**
   * {@inheritdoc}
   */
  public function addModule($name, $path) {
    return $this->moduleHandler->addModule($name, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function addProfile($name, $path) {
    return $this->moduleHandler->addProfile($name, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function buildModuleDependencies(array $modules) {
    return $this->moduleHandler->buildModuleDependencies($modules);
  }

  /**
   * {@inheritdoc}
   */
  public function moduleExists($module) {
    return $this->moduleHandler->moduleExists($module);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllIncludes($type, $name = NULL) {
    return $this->moduleHandler->loadAllIncludes($type, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function loadInclude($module, $type, $name = NULL) {
    return $this->moduleHandler->loadInclude($module, $type, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getHookInfo() {
    return $this->moduleHandler->getHookInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function getImplementations($hook) {
    return $this->moduleHandler->getImplementations($hook);
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache() {
    return $this->moduleHandler->writeCache();
  }

  /**
   * {@inheritdoc}
   */
  public function resetImplementations() {
    return $this->moduleHandler->resetImplementations();
  }

  /**
   * {@inheritdoc}
   */
  public function implementsHook($module, $hook) {
    return $this->moduleHandler->implementsHook($module, $hook);
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($module, $hook, array $args = array()) {
    return $this->moduleHandler->invoke($module, $hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAll($hook, array $args = array()) {
    if (preg_match($this->hookPattern, $hook)) {
      foreach (array_values($args) as $key => $arg) {
        if ($arg instanceof FieldableEntityInterface) {
          $this->crawler->handle($hook . ($key ? "/$key/" : ''), $arg);
        }
      }
    }
    return $this->moduleHandler->invokeAll($hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    return $this->moduleHandler->alter($type, $data, $context1, $context2);
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDirectories() {
    return $this->moduleHandler->getModuleDirectories();
  }

  /**
   * {@inheritdoc}
   */
  public function getName($module) {
    return $this->moduleHandler->getName($module);
  }

}
