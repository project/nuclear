<?php

namespace Drupal\nuclear;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class NuclearPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/nuclear', $namespaces, $module_handler, 'Drupal\nuclear\NuclearPluginInterface', 'Drupal\Component\Annotation\PluginID');

    $this->alterInfo('nuclear');
    $this->setCacheBackend($cache_backend, 'nuclear_plugins');
  }

}
