<?php

namespace Drupal\nuclear\Plugin\nuclear;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Queue\QueueInterface;
use Drupal\nuclear\NuclearPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @PluginID("queue")
 */
class Queue extends PluginBase implements NuclearPluginInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Queue constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Queue\QueueInterface $queue
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, QueueInterface $queue) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queue = $queue;
  }

  /*
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $configuration += ['reliable' => TRUE];
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue')->get($configuration['queue'], $configuration['reliable'])
    );
  }

  /*
   * {@inheritdoc}
   */
  public function react(FieldableEntityInterface $entity, $field_name, $all_entities, $path) {
    $this->queue->createItem(['type' => $entity->getEntityTypeId(), 'id' => $entity->id()]);
  }

}
