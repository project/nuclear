<?php

namespace Drupal\nuclear;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

interface NuclearPluginInterface extends PluginInspectionInterface {

  /**
   * @param $entity
   * @param $field_name
   * @param \Drupal\Core\Entity\FieldableEntityInterface[] $all_entities
   * @param $path
   *  The current entity tree path. $all_entities[$path] is $entity.
   */
  public function react(FieldableEntityInterface $entity, $field_name, $all_entities, $path);

}
