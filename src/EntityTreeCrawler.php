<?php

namespace Drupal\nuclear;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * This doxygen is written to appease the coding standards.
 *
 * I do not have the time to write good doxygen right now.
 */
class EntityTreeCrawler {

  /**
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The entities seen keyed by entity type and entity id.
   *
   * @var \Drupal\Core\Entity\FieldableEntityInterface[]
   */
  protected $seen;

  /**
   * The entities seen keyed by path.
   *
   * @var \Drupal\Core\Entity\FieldableEntityInterface[]
   */
  protected $entities;

  /**
   * EntityTreeCrawler constructor.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface
   */
  public function __construct(PluginManagerInterface $plugin_manager = NULL) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * @param $path
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   * @param int $depth
   */
  public function handle($path, FieldableEntityInterface $entity, $depth = 0) {
    if (!$depth) {
      $this->seen = [];
      $this->entities = [];
    }
    if (isset($this->seen[$entity->getEntityTypeId()][$entity->id()])) {
      return;
    }
    $this->seen[$entity->getEntityTypeId()][$entity->id()] = TRUE;
    $this->entities[$path] = $entity;
    foreach ($entity->getFieldDefinitions() as $field_name => $field_definition) {
      if ($field_definition instanceof ThirdPartySettingsInterface && ($setting = $field_definition->getThirdPartySetting('nuclear', $path))) {
        $this->getPluginManager()->createInstance($setting['plugin'], $setting)
          ->react($entity, $field_name, $this->entities, $path);
      }
      if (is_a($field_definition->getItemDefinition()->getClass(), EntityReferenceItem::class, TRUE)) {
        foreach ($entity->get($field_name) as $item) {
          $target_entity = $item->entity;
          if ($target_entity instanceof FieldableEntityInterface) {
            $this->handle("$path/$field_name", $target_entity, $depth + 1);
          }
        }
      }
    }
  }

  /**
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected function getPluginManager() {
    if (!isset($this->pluginManager)) {
      $this->pluginManager = \Drupal::service('nuclear.plugin_manager');
    }
    return $this->pluginManager;
  }

}
